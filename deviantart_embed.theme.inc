<?php

// $Id $

function theme_deviantart_embed_default($field, $item, $formatter, $node) {
  if ($item['value'] && $item['provider']) {
    $code = $item['value'];
    $link = $field['widget']['full_link'] ? $field['widget']['full_link'] : variable_get('deviantart_embed_default_link', DEVIANTART_EMBED_DEFAULT_FULL_LINK);
    if ($link == DEVIANTART_EMBED_LINK_CONTENT) {
      $link = 'node/'. $node->nid;
    }
    else if ($link == DEVIANTART_EMBED_LINK_PROVIDER) {
      $link = module_invoke('emfield', 'include_invoke', 'deviantart_embed', $item['provider'], 'embedded_link', $code, $item['data']);
    }
    else {
      $link = NULL;
    }
    $title = module_invoke('emfield', 'include_invoke', 'deviantart_embed', $item['provider'], 'image_title', $code, $item['data']);
    $url = module_invoke('emfield', 'include_invoke', 'deviantart_embed', $item['provider'], 'image_url', $code, $width, $height, $formatter, $field, $item, $node);
    
    $embedcode = '<object>
  <embed src="http://backend.deviantart.com/embed/view.swf" 
    type="application/x-shockwave-flash" 
    width="450" 
    flashvars="id='. $item['value'] .'" 
    height="540" 
    allowscriptaccess="always">
  </embed>
</object>';
    if ($link) {
      $output = l($output, $link, array('html' => true));
  	}
  	
  	$output = $embedcode;
  }
  return $output;
}

function theme_deviantart_embed_formatter_default($element) {
  $field = content_fields($element['#field_name'], $element['#type_name']);
  return module_invoke('deviantart_embed', 'field_formatter', $field, $element['#item'], $element['#formatter'], $element['#node']);
}