<?php
// $Id $

define('DEVIANTART_MAIN_URL', 'http://deviantart.com/');

/**
 * hook deviantart_embed_PROVIDER_info
 * this returns information relevant to a devinatART
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */

function deviantart_embed_deviantart_info() {
  $features = array();
  return array(
    'provider' => 'deviantart',
    'name' => t('deviantART'),
    'url' => DEVIANTART_MAIN_URL,
    'settings_description' => t('These settings specifically affect images displayed from <a href="@deviantarturl" target="_blank">deviantART.com</a>.', array('@deviantarturl' => DEVIANTART_MAIN_URL)),
    'supported_features' => $features,
  );
}

function deviantart_embed_deviantart_data($field, $item) {
  $data = array();

  $url = $item['embed'];
  if (preg_match('!([^/.:@]+)\.deviantart\.com/art/?(.+)$!i', $url, $matches)) {
  	$artist = $matches[1];
  	$tokens = explode("-", $matches[2]);
  	$count = count($tokens);
  	$embedid = $tokens[$count-1];
    $data = array(
      'artist' => $artist,
      'tokens' => $matches[2],
      'embedid' => $embedid,
      'title' => '',
    );
  }
  return $data;
}

function deviantart_embed_deviantart_extract($embed = '') {
  // http://vyoma.deviantart.com/art/Fire-Sorceress-106653439
  if (preg_match('!([^/.:@]+)\.deviantart\.com/art/?(.+)$!i', $embed, $matches)) {
  	$tokens = explode("-", $matches[2]);
  	$count = count($tokens);
  	$embedid = $tokens[$count-1];
    return $embedid;
  }

  return array();
}

/**
 * hook deviantart_embed_PROVIDER_embedded_link($code)
 * returns a link to view the content at the provider's site
 *  @param $code
 *    the string containing the content to watch
 *  @return
 *    a string containing the URL to view the image at the original provider's site
 */
function deviantart_embed_deviantart_embedded_link($code, $data) {
  return "http://{$data['artist']}.deviantart.com/art/{$data['tokens']}";
}

/**
 *  implement deviantart_embed_PROVIDER_image_url
 *
 *  @param $code
 *    the code of the image
 *  @param $data
 *    any stored data for the image, which may already have the title
 *  @return
 *    the url directly to the image to display
 */
function deviantart_embed_deviantart_image_url($code, $data) {
  if (func_num_args() == 7) {
    $arg = func_get_arg(5);
    $code = &$arg['data']['file'];
    $data = &$arg['data'];
  }
   return "http://{$data['artist']}.deviantart.com/art/{$data['tokens']}";
}

/**
 *  implement deviantart_embed_PROVIDER_image_title
 *
 *  @param $code
 *    the code of the image
 *  @param $data
 *    any stored data for the image, which may already have the title
 *  @return
 *    the title as the 3rd party provider knows it, if accessible to us. otherwise, ''
 */
function deviantart_embed_deviantart_image_title($code, $data) {
  return ''; // Not provided by on deviantART.
}